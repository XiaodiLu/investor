package com.citi.trading;


import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.hasEntry;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

import java.util.function.Consumer;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import static org.mockito.Mockito.any;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

public class InvestorTesting {
	private Market market;
	private Investor investor;
	
//	public synchronized void placeOrder(Trade order, Consumer<Trade> callback) {
//		callback.accept(order);
//	}
	@Before
	public void setUp() {
		market = mock(Market.class);
		investor = new Investor(10000);
	}
	
	@Test
	public void testBuy() {
		
		Mockito.doAnswer(new Answer<Object>() {
			@Override
			public Object answer(InvocationOnMock a) throws Throwable {
				Trade order = a.getArgument(0);
				Consumer<Trade> c = a.getArgument(1);
				c.accept(order);
				return null;
			}
		}).when(market).placeOrder( any(Trade.class), any(Consumer.class));;
		investor.setMarket(market);
		investor.buy("MRK", 100, 60);
		assertThat (investor.getPortfolio(), hasEntry ("MRK", 100));
		assertThat(investor.getCash(), equalTo(4000.0));

	}
	@Test
	public void testSell() {
		
		Mockito.doAnswer(new Answer<Object>() {
			@Override
			public Object answer(InvocationOnMock a) throws Throwable {
				Trade order = a.getArgument(0);
				Consumer<Trade> c = a.getArgument(1);
				c.accept(order);
				return null;
			}
		}).when(market).placeOrder( any(Trade.class), any(Consumer.class));;
		investor.setMarket(market);
		investor.buy("MRK", 100, 60);
		investor.buy("APPLE", 100, 20);
		investor.sell("MRK", 100, 60);
		assertThat (investor.getPortfolio(), hasEntry ("APPLE", 100));
		assertThat (investor.getPortfolio(), hasEntry ("MRK", 0));
		assertThat(investor.getCash(), equalTo(8000.0));

	}

}
